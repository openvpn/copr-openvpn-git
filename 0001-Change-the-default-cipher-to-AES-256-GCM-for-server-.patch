From d045acbbdd0c4683903f84eb866087511a69c687 Mon Sep 17 00:00:00 2001
From: David Sommerseth <dazo@eurephia.org>
Date: Mon, 5 Dec 2022 20:51:25 +0100
Subject: [PATCH] Change the default cipher to AES-256-GCM for server
 configurations

This change makes the server use AES-256-GCM instead of BF-CBC as the default
cipher for the VPN tunnel.  To avoid breaking existing running configurations
defaulting to BF-CBC, the Negotiable Crypto Parameters (NCP) list contains
the BF-CBC in addition to AES-CBC.  This makes it possible to migrate
existing older client configurations one-by-one to use at least AES-CBC unless
the client is updated to v2.4 (which defaults to upgrade to AES-GCM automatically)

[Update 2022-06-10]
The BF-CBC reference is now removed as of Fedora 36 and newer.  The Blowfish
cipher is no longer available by default in OpenSSL 3.0.  It can be enabled
via the legacy provider in OpenSSL 3.0, but BF-CBC is deprecated and should
not be used any more.  OpenVPN 2.4 and newer will always negotiate a stronger
cipher by default and older OpenVPN releases are no longer supported upstream.

---
 distro/systemd/openvpn-server@.service.in | 2 +-
 1 file changed, 1 insertion(+), 1 deletion(-)

diff --git a/distro/systemd/openvpn-server@.service.in b/distro/systemd/openvpn-server@.service.in
index 6e8e7d9..6acbc8e 100644
--- a/distro/systemd/openvpn-server@.service.in
+++ b/distro/systemd/openvpn-server@.service.in
@@ -10,7 +10,7 @@
 Type=notify
 PrivateTmp=true
 WorkingDirectory=/etc/openvpn/server
-ExecStart=@sbindir@/openvpn --status %t/openvpn-server/status-%i.log --status-version 2 --suppress-timestamps --config %i.conf
+ExecStart=@sbindir@/openvpn --status %t/openvpn-server/status-%i.log --status-version 2 --suppress-timestamps --cipher AES-256-GCM --data-ciphers AES-256-GCM:AES-128-GCM:AES-256-CBC:AES-128-CBC --config %i.conf
 CapabilityBoundingSet=CAP_IPC_LOCK CAP_NET_ADMIN CAP_NET_BIND_SERVICE CAP_NET_RAW CAP_SETGID CAP_SETUID CAP_SETPCAP CAP_SYS_CHROOT CAP_DAC_OVERRIDE CAP_AUDIT_WRITE
 LimitNPROC=10
 DeviceAllow=/dev/null rw
