#!/bin/bash

set -eux

[ -d openvpn ] || git clone https://github.com/OpenVPN/openvpn.git
commit=$(git -C openvpn log -n1 --pretty="format:%H")
commit_abbrev=$(git -C openvpn log -n1 --pretty="format:%h" --abbrev=7)
commit_date=$(git -C openvpn log -n1 --pretty="format:%cd" --date="format:%Y%m%d")

sed -i \
    -e "s/%global snap_tstamp .*\$/%global snap_tstamp $commit_date/" \
    -e "s/%global commit .*\$/%global commit $commit/" \
    openvpn.spec

if ! git diff --exit-code; then
    changelog_date=$(LANG=en_US date "+%a %b %d %Y")
    changelog_author="$(git config user.name) <$(git config user.email)>"
    changelog_entry="%changelog\n* $changelog_date $changelog_author - 2.7.${commit_date}git${commit_abbrev}\n- Update to git master $commit\n"
    sed -i \
	-e "s/%changelog/$changelog_entry/" \
	openvpn.spec
    git commit -a -m "Update to git master $commit"
fi
