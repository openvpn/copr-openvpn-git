%define _hardened_build 1
%global snap_tstamp 20250306
%global commit 7e5535289908af23bed244e7f0417358f2f316bc
%global shortcommit  %(c=%{commit}; echo ${c:0:7})

# LTO needs to be disabled to avoid issues on Fedora 34+ and
# EL-9 when linking the unit-test, which utilizes --wrap in
# the link process
%if 0%{?rhel} > 8 || 0%{?fedora} > 34
%global _lto_cflags %{nil}
%endif

# The DCO feature is only available on EL-8+ or Fedora 34+
%if 0%{?rhel} > 7 || 0%{?fedora} > 34
%bcond_without dco
%else
%bcond_with dco
%endif

# pkcs11-helper on RHEL9 (v1.27.0) comes with a buggy pkcs11.h, so skip it
%if 0%{?rhel} == 9
%bcond_with pkcs11
%else
%bcond_without pkcs11
%endif

# Build conditionals
# tests_long - Enabled by default, enables long running tests in %%check
%bcond_without tests_long

Name:              openvpn
Version:           2.7
Release:           %{snap_tstamp}git%{shortcommit}%{?dist}
Summary:           A full-featured TLS VPN solution
URL:               https://community.openvpn.net/
Source0:           https://github.com/OpenVPN/openvpn/archive/%{commit}/openvpn-%{shortcommit}.tar.gz
Source2:           roadwarrior-server.conf
Source3:           roadwarrior-client.conf
Source11:          openvpn.rpmlintrc
Patch1:            0001-Change-the-default-cipher-to-AES-256-GCM-for-server-.patch
Patch2:            fedora-crypto-policy-compliance.patch
Patch50:           openvpn-2.4-change-tmpfiles-permissions.patch
License:           GPLv2
BuildRequires:     gcc
BuildRequires:     automake
BuildRequires:     autoconf
BuildRequires:     autoconf-archive
BuildRequires:     libcap-ng-devel
BuildRequires:     libtool
BuildRequires:     gettext
BuildRequires:     lzo-devel
BuildRequires:     lz4-devel
BuildRequires:     make
BuildRequires:     openssl-devel >= 1.1.0
%if %{with dco}
BuildRequires:     libnl3-devel
%endif
%if %{with pkcs11}
BuildRequires:     pkcs11-helper-devel >= 1.11
%endif
BuildRequires:     pam-devel
BuildRequires:     libselinux-devel
BuildRequires:     libcmocka-devel
BuildRequires:     systemd
BuildRequires:     systemd-devel

%{?systemd_requires}
Requires(pre):     /usr/sbin/useradd

%if %{with dco}
Recommends:        kmod-ovpn-dco >= 0.2
%endif

%if 0%{?rhel} > 7 || 0%{?fedora} > 34
BuildRequires:  python3-docutils
%else
# We cannot use python36-docutils on RHEL-7 as
# the ./configure script does not currently find
# the rst2man-3 executable, it only looks for rst2man
BuildRequires:  python-docutils
%endif

# For the perl_default_filter macro
BuildRequires:     perl-macros

# Filter out the perl(Authen::PAM) dependency.
# No perl dependency is really needed at all.
%{?perl_default_filter}

%description
OpenVPN is a robust and highly flexible tunneling application that uses all
of the encryption, authentication, and certification features of the
OpenSSL library to securely tunnel IP networks over a single UDP or TCP
port.  It can use the Marcus Franz Xaver Johannes Oberhumers LZO library
for compression.

%package devel
Summary:           Development headers and examples for OpenVPN plug-ins

%description devel
OpenVPN can be extended through the --plugin option, which provides
possibilities to add specialized authentication, user accounting,
packet filtering and related features.  These plug-ins need to be
written in C and provides a more low-level and information rich access
to similar features as the various script-hooks.

%prep
%setup -q -n openvpn-%{commit}
%patch -P 1 -p1
%if 0%{?rhel} > 7 || 0%{?fedora} > 34
# The crypto-policy patch is only valid on RHEL-8 and newer plus Fedora
%patch -P 2 -p1
%endif
%patch -P 50 -p1

echo "define([PRODUCT_NAME], [OpenVPN])" > version.m4
echo "define([PRODUCT_VERSION], [%{version}-%{release}])" >> version.m4
echo "define([PRODUCT_GUIVERSION], [copr:%{version}-%{release}])" >> version.m4
echo "define([PRODUCT_TARNAME], [openvpn])" >> version.m4
echo "define([PRODUCT_BUGREPORT], [openvpn-devel@lists.sourceforge.net])" >> version.m4
echo "define([PRODUCT_VERSION_MAJOR], [2])" >> version.m4
echo "define([PRODUCT_VERSION_MINOR], [7])" >> version.m4
echo "define([PRODUCT_VERSION_PATCH], [_git%{shortcommit}])" >> version.m4

autoreconf -vi

# %%doc items shouldn't be executable.
find contrib sample -type f -perm /100 \
    -exec chmod a-x {} \;

%build
%configure \
    --enable-silent-rules \
    --with-crypto-library=openssl \
    %{?with_pkcs11:--enable-pkcs11} \
    --enable-selinux \
    --enable-systemd \
    --enable-x509-alt-username \
    --enable-async-push \
    %{?!with_dco:--disable-dco} \
    --docdir=%{_pkgdocdir} \
    SYSTEMD_UNIT_DIR=%{_unitdir} \
    TMPFILES_DIR=%{_tmpfilesdir}

%{__make} %{?_smp_mflags}


%check
# Test Crypto:
./src/openvpn/openvpn --genkey secret key
./src/openvpn/openvpn --cipher aes-128-cbc --test-crypto --secret key --allow-deprecated-insecure-static-crypto
./src/openvpn/openvpn --cipher aes-256-cbc --test-crypto --secret key --allow-deprecated-insecure-static-crypto
./src/openvpn/openvpn --cipher aes-128-gcm --test-crypto --secret key --allow-deprecated-insecure-static-crypto
./src/openvpn/openvpn --cipher aes-256-gcm --test-crypto --secret key --allow-deprecated-insecure-static-crypto

# Some of the unit tests does not run on RHEL-7
pushd tests/unit_tests
%{__make} %{?_smp_mflags} check
popd

%if %{with tests_long}
# Randomize ports for tests to avoid conflicts on the build servers.
cport=$[ 50000 + ($RANDOM % 15534) ]
sport=$[ $cport + 1 ]
sed -e 's/^\(rport\) .*$/\1 '$sport'/' \
    -e 's/^\(lport\) .*$/\1 '$cport'/' \
    < sample/sample-config-files/loopback-client \
    > %{_tmppath}/%{name}-%{version}-%{release}-%(%{__id_u})-loopback-client
sed -e 's/^\(rport\) .*$/\1 '$cport'/' \
    -e 's/^\(lport\) .*$/\1 '$sport'/' \
    < sample/sample-config-files/loopback-server \
    > %{_tmppath}/%{name}-%{version}-%{release}-%(%{__id_u})-loopback-server

pushd sample
# Test SSL/TLS negotiations (runs for 2 minutes):
../src/openvpn/openvpn --config \
    %{_tmppath}/%{name}-%{version}-%{release}-%(%{__id_u})-loopback-client &
../src/openvpn/openvpn --config \
    %{_tmppath}/%{name}-%{version}-%{release}-%(%{__id_u})-loopback-server
wait
popd

rm -f %{_tmppath}/%{name}-%{version}-%{release}-%(%{__id_u})-loopback-client \
    %{_tmppath}/%{name}-%{version}-%{release}-%(%{__id_u})-loopback-server
%endif

%install
%{__make} install DESTDIR=$RPM_BUILD_ROOT
find $RPM_BUILD_ROOT -name '*.la' | xargs rm -f
mkdir -p -m 0750 $RPM_BUILD_ROOT%{_sysconfdir}/%{name}/client $RPM_BUILD_ROOT%{_sysconfdir}/%{name}/server
cp %{SOURCE2} %{SOURCE3} sample/sample-config-files/

# Create some directories the OpenVPN package should own
mkdir -m 0750 -p $RPM_BUILD_ROOT%{_rundir}/%{name}-{client,server}
mkdir -m 0770 -p $RPM_BUILD_ROOT%{_sharedstatedir}/%{name}

# Package installs into %%{_pkgdocdir} directly
# Add various additional files
cp -a AUTHORS ChangeLog contrib sample distro/systemd/README.systemd $RPM_BUILD_ROOT%{_pkgdocdir}

# Fix incorrect she-bang on a python script
# https://docs.fedoraproject.org/en-US/packaging-guidelines/Python/#_interpreter_invocation
sed -e "s|^#!/usr/bin/env.*python3$|#!%{python3} -%{py3_shebang_flags}|" \
    -i $RPM_BUILD_ROOT%{_pkgdocdir}/contrib/extract-crl/extractcrl.py

# Remove some files which does not really belong here
rm -f  $RPM_BUILD_ROOT%{_pkgdocdir}/sample/Makefile{,.in,.am}
rm -f  $RPM_BUILD_ROOT%{_pkgdocdir}/contrib/multilevel-init.patch
rm -rf $RPM_BUILD_ROOT%{_pkgdocdir}/sample/sample-keys

# Remove totpauth.py on RHEL-7, as it is not able to process the code
%if 0%{?rhel} == 7
rm -f $RPM_BUILD_ROOT%{_pkgdocdir}/sample/sample-scripts/totpauth.py
%endif


%pre
getent group openvpn &>/dev/null || groupadd -r openvpn
getent passwd openvpn &>/dev/null || \
    /usr/sbin/useradd -r -g openvpn -s /sbin/nologin -c OpenVPN \
        -d /etc/openvpn openvpn
exit 0

%post
for srv in `systemctl | awk '/openvpn-client@.*\.service/{print $1} /openvpn-server@.*\.service/{print $1}'`;
do
    %systemd_post $srv
done

%preun
for srv in `systemctl | awk '/openvpn-client@.*\.service/{print $1} /openvpn-server@.*\.service/{print $1}'`;
do
    %systemd_preun $srv
done

%postun
for srv in `systemctl | awk '/openvpn-client@.*\.service/{print $1} /openvpn-server@.*\.service/{print $1}'`;
do
    %systemd_postun_with_restart $srv
done

%files
%{_pkgdocdir}
%exclude %{_pkgdocdir}/README.mbedtls
%exclude %{_pkgdocdir}/sample/sample-plugins
%{_mandir}/man8/%{name}.8*
%{_mandir}/man5/%{name}-*.5*
%{_sbindir}/%{name}
%{_libdir}/%{name}/
%{_unitdir}/%{name}-client@.service
%{_unitdir}/%{name}-server@.service
%{_tmpfilesdir}/%{name}.conf
%config %dir %{_sysconfdir}/%{name}/
%config %dir %attr(-,-,openvpn) %{_sysconfdir}/%{name}/client
%config %dir %attr(-,-,openvpn) %{_sysconfdir}/%{name}/server
%attr(0770,openvpn,openvpn) %{_sharedstatedir}/%{name}
%dir %attr(0750,-,openvpn) %{_rundir}/openvpn-client
%dir %attr(0750,-,openvpn) %{_rundir}/openvpn-server

%files devel
%{_pkgdocdir}/sample/sample-plugins
%{_includedir}/openvpn-plugin.h
%{_includedir}/openvpn-msg.h


%changelog
* Fri Mar 07 2025 OpenVPN CI <pkg@openvpn.net> - 2.7.20250306git7e55352
- Update to git master 7e5535289908af23bed244e7f0417358f2f316bc

* Thu Mar 06 2025 OpenVPN CI <pkg@openvpn.net> - 2.7.20250305gitfed37d0
- Update to git master fed37d003d9cbc9e7c3ea5fdd0f0a287f4cee538

* Sat Mar 01 2025 OpenVPN CI <pkg@openvpn.net> - 2.7.20250228git30a66f7
- Update to git master 30a66f72a98d7a583c524f826a94c4d3e5081688

* Fri Feb 21 2025 OpenVPN CI <pkg@openvpn.net> - 2.7.20250220gitbca57c1
- Update to git master bca57c1d9c82c890f1b16586b416fabd4f9b1e56

* Thu Feb 20 2025 OpenVPN CI <pkg@openvpn.net> - 2.7.20250219git8720bc8
- Update to git master 8720bc8e2e4fbccd16ca4bffd27c416575b1bfee

* Sun Feb 16 2025 OpenVPN CI <pkg@openvpn.net> - 2.7.20250215git43abb41
- Update to git master 43abb412dd211836086e27b55f1bcd77626ad6f2

* Sat Feb 15 2025 OpenVPN CI <pkg@openvpn.net> - 2.7.20250214git6248e0a
- Update to git master 6248e0af25a936728745a239bc99a390fb774a7c

* Fri Feb 14 2025 OpenVPN CI <pkg@openvpn.net> - 2.7.20250213git666e440
- Update to git master 666e440be8007fda5e6277de9298fff67a76738f

* Thu Feb 13 2025 OpenVPN CI <pkg@openvpn.net> - 2.7.20250212git9f4670f
- Update to git master 9f4670fc718a8c16280d60c3e2d4ad29cffa04c4

* Thu Feb 06 2025 OpenVPN CI <pkg@openvpn.net> - 2.7.20250205git817577d
- Update to git master 817577d56ba48e160d4c7ad939ce62a93b55ab7f

* Tue Feb 04 2025 OpenVPN CI <pkg@openvpn.net> - 2.7.20250203git060ead6
- Update to git master 060ead650450553214dad3e9856c500deb672bba

* Mon Feb 03 2025 OpenVPN CI <pkg@openvpn.net> - 2.7.20250201gite6b2df0
- Update to git master e6b2df0ce618f1fe1f28bbd287b3580147ce34b4

* Sun Feb 02 2025 OpenVPN CI <pkg@openvpn.net> - 2.7.20250201git800e8ab
- Update to git master 800e8abdecd6cab62fd2074594b4e08aa72d0d82

* Sat Feb 01 2025 OpenVPN CI <pkg@openvpn.net> - 2.7.20250131gitbfa5f0b
- Update to git master bfa5f0bdfe89d181f7b5f7af2ba34ab9357a1d13

* Thu Jan 30 2025 OpenVPN CI <pkg@openvpn.net> - 2.7.20250129git9df51cf
- Update to git master 9df51cf56e27f6103ed5becbafd1f645726d9155

* Wed Jan 29 2025 OpenVPN CI <pkg@openvpn.net> - 2.7.20250128git34f3022
- Update to git master 34f302247c865827b7075422e32c272a203783cb

* Tue Jan 28 2025 OpenVPN CI <pkg@openvpn.net> - 2.7.20250127git40518dc
- Update to git master 40518dc66d9d04e6ec7e04439d7a6bc7fd6ac20f

* Sun Jan 26 2025 OpenVPN CI <pkg@openvpn.net> - 2.7.20250125git2a50daf
- Update to git master 2a50daf5fe6c924c8d945b3d0455c9e282cd769f

* Sat Jan 25 2025 OpenVPN CI <pkg@openvpn.net> - 2.7.20250124git8a0f297
- Update to git master 8a0f297e068b6701acceafc5af92eddd18a4151b

* Fri Jan 24 2025 OpenVPN CI <pkg@openvpn.net> - 2.7.20250123git8466c2c
- Update to git master 8466c2ca3faf0dc143262ea2a76bfe3e2aff9f51

* Sat Jan 18 2025 OpenVPN CI <pkg@openvpn.net> - 2.7.20250117git5cea4bc
- Update to git master 5cea4bc75e1b87ec7225a89b7cf5cadb85e77e8a

* Thu Jan 16 2025 OpenVPN CI <pkg@openvpn.net> - 2.7.20250115git6d91eda
- Update to git master 6d91eda5a120a4a18273d82fed6777b3efcce8cc

* Wed Jan 15 2025 OpenVPN CI <pkg@openvpn.net> - 2.7.20250114git758d281
- Update to git master 758d28122954a30102f44b49e43f4b950553448a

* Tue Jan 14 2025 OpenVPN CI <pkg@openvpn.net> - 2.7.20250113git5e086c0
- Update to git master 5e086c08f2ce4428fd014b74441f0197a71d6da8

* Sat Jan 11 2025 OpenVPN CI <pkg@openvpn.net> - 2.7.20250110git0671a4d
- Update to git master 0671a4d009ae89870b645d1a8f3078d9943d5010

* Fri Jan 10 2025 OpenVPN CI <pkg@openvpn.net> - 2.7.20250109gitbc62a9a
- Update to git master bc62a9a02cb7365a678bcd3f2faf537a420cc5a0

* Wed Jan 01 2025 OpenVPN CI <pkg@openvpn.net> - 2.7.20241231git6a7931a
- Update to git master 6a7931a4a89cb35be7b799942e7fa03fde2cdc63

* Tue Dec 31 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20241230git7fac371
- Update to git master 7fac3716a9cb4d0c104d18b4dc939d73736a4ef3

* Mon Dec 30 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20241229git10e785f
- Update to git master 10e785f1dcacdfd5240ac18a53f2839b4d3b979c

* Sat Dec 28 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20241227git115058d
- Update to git master 115058d2d586ad19956d7ee119ca438b1c5a985a

* Thu Dec 26 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20241225gitbd629d7
- Update to git master bd629d7c8b389bdece3c195552acf4343021b9b8

* Wed Dec 25 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20241224git82fd89a
- Update to git master 82fd89a04f774eeb785ad110dd6923a12345f973

* Tue Dec 24 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20241223gitf0c26b0
- Update to git master f0c26b02a7e394287052d524ef6d6bc738635692

* Sun Dec 22 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20241222git435cc9a
- Update to git master 435cc9a7535c86c1ab513ae106895960c183095e

* Sat Dec 21 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20241221gitdb46d4d
- Update to git master db46d4d38bec59435a76bb0968d530b9426c551e

* Fri Dec 20 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20241220git6f2d222
- Update to git master 6f2d222c92bb4555dc931c2aed3510a64bd53df3

* Thu Dec 12 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20241212gitbaa9192
- Update to git master baa9192851006e2dbb90b410011e61ecf2e01870

* Wed Dec 11 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20241211git387c207
- Update to git master 387c2076af14a0f1ba97b6ca0175d81d1e8391a5

* Mon Dec 09 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20241209gitae82631
- Update to git master ae82631be17ca8c49d356510c9e7709ddcf19db3

* Sun Dec 08 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20241208git9343cd1
- Update to git master 9343cd101d74377305b96cd501b8659f64c70e46

* Fri Dec 06 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20241206git91b6e28
- Update to git master 91b6e288a49ee57a4eeeff3f22f2440c94594a1d

* Thu Dec 05 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20241205git7da1492
- Update to git master 7da1492507ef20f66dcbb5f6f9bc5b32b2d82b56

* Thu Nov 28 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20241128gitcb87888
- Update to git master cb878882388bfe9dc49b116190c5c6ae8918322d

* Tue Nov 26 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20241126git6c636f5
- Update to git master 6c636f5387245ceb4809eac765c4b649709b4aa8

* Mon Nov 11 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20241111gitd52ea24
- Update to git master d52ea247d9dec5262a09f3891db83b79b2ca403e

* Sat Nov 09 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20241109git648e160
- Update to git master 648e1606496adc6ec37a9d862810e465dab64dc0

* Wed Nov 06 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20241105gitd5b4713
- Update to git master d5b4713153ce95e88651d93aa1ef25b2676d0cfc

* Tue Nov 05 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20241105gitdee0748
- Update to git master dee0748a1e0f57c326cf2b83f8499998ac9d1187

* Mon Oct 28 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20241028gita7f80d4
- Update to git master a7f80d402fb95df3c58a8fc5d12cdb8f39c37d3e

* Sat Oct 26 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20241026git5dd1b8b
- Update to git master 5dd1b8b06335831206077a1eb4aa81c3ceb3f3ee

* Fri Oct 25 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20241025git37db7fe
- Update to git master 37db7fe37afb555de651314a04c537fd7fbaa280

* Thu Oct 24 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20241024gita3a4844
- Update to git master a3a4844a3c558eeba31f45b5a4ddcdd8b76ab897

* Wed Oct 23 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20241023git5861274
- Update to git master 58612741680994928e67a0e981c91bbffb7a2d20

* Fri Oct 18 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20241018git8ae409a
- Update to git master 8ae409ad72a980d79dd8d40ab4284c3fab1efeb9

* Thu Oct 17 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20241017gitbacdbbe
- Update to git master bacdbbee7e2c0c1114b9f5e19b124f91680fd937

* Wed Oct 16 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20241016git33a700d
- Update to git master 33a700d61e5b1a724b00f17fa0ed75d694e2823a

* Sat Sep 28 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20240928git5ccaa49
- Update to git master 5ccaa49e0323ce468b156b3fa0930f7840050387

* Fri Sep 27 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20240927git95e5a0b
- Update to git master 95e5a0b4f0f0f45d29a5a995f151677b760ddf76

* Wed Sep 25 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20240925gita8cc97f
- Update to git master a8cc97f94f3e7bb52270ca2acf89804093535839

* Tue Sep 24 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20240924git5c4a0b7
- Update to git master 5c4a0b71abecf1ccfff4c2ddadf0db9818b40f36

* Mon Sep 23 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20240922gitd849073
- Update to git master d8490738e64a5bbf00d6f08af03fef58f7488140

* Thu Sep 19 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20240919gitb322690
- Update to git master b322690394b75a9d4987d4b27107ccb01bbcd90e

* Wed Sep 18 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20240918gitb59620f
- Update to git master b59620f24f5bc853e2aa92943f14abf74aa81511

* Tue Sep 17 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20240917gitb236261
- Update to git master b236261f9ce14bc9fffbb81c61938e0e24b200d6

* Mon Sep 16 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20240916gitac3a7fd
- Update to git master ac3a7fd93542420f12d58e5c7490076b5741fb5a

* Thu Sep 12 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20240912git6598590
- Update to git master 65985905c5abc69c1ee34c4cab6fdf8b73da7f50

* Wed Sep 11 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20240911gitf4d7cec
- Update to git master f4d7cec855aea3c453b75fe68a1c151400793661

* Tue Sep 10 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20240910git53449cb
- Update to git master 53449cb61ff569c4862926c7999d50f634030fd9

* Mon Sep 09 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20240909git32e748b
- Update to git master 32e748bd3320cd07b9e7671ee0cec95f4fd85935

* Sun Sep 08 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20240908git0fe3a98
- Update to git master 0fe3a9877468ecfb3b97c67ecca5495eed7a8683

* Fri Sep 06 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20240906gitdac076f
- Update to git master dac076fe406adace826766f6cc3cfdadc5f06be4

* Tue Aug 27 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20240814git233e10a
- Update to git master 233e10aeec7de02d34fa5c517b44612d38ccc00f

* Fri Jul 26 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20240726git418463a
- Update to git master 418463ad27c13f56adb5b02cfd62018b7d634ee8

* Thu Jul 25 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20240725git8991f0d
- Update to git master 8991f0d5c6c06d1e42919d1d6a0813ca1c46f8a1

* Mon Jul 22 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20240722git856065b
- Update to git master 856065b2eb37286c389550593472bf180bc5be9d

* Wed Jul 17 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20240717git37b696a
- Update to git master 37b696a207548df88fe65aa130fe6d522e7ce920

* Thu Jul 04 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20240704gitf8f4771
- Update to git master f8f477139804b06183b515a529c982f524547d18

* Wed Jun 26 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20240626git0ea5126
- Update to git master 0ea51261d096b54281287bbd2a6899041c4dbd43

* Fri Jun 21 2024 Frank Lichtenheld <frank@lichtenheld.com> - 2.7.20240620git5635592
- Change /run directories from ghost to dir (RHBZ#2281686)

* Thu Jun 20 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20240620git5635592
- Update to git master 56355924b4945ec808500b18c714c111387697f9

* Wed Jun 19 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20240619git414f428
- Update to git master 414f428fa29694090ec4c46b10a8aba419c85659

* Tue Jun 18 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20240618git06c7ce5
- Update to git master 06c7ce5d1fc3b17e0da731d22002e58b9e2d4994

* Fri Jun 07 2024 Frank Lichtenheld <frank@lichtenheld.com> - 2.7.20240606git13ee7f9
- Remove patch for --enable-systemd breaking UT build. Merged upstream

* Thu Jun 06 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20240606git13ee7f9
- Update to git master 13ee7f902f18e27b981f8e440facd2e6515c6c83

* Wed Jun 05 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20240605gitbf887c9
- Update to git master bf887c95e46c6892ac1f68be5559525f8d975530

* Tue Jun 04 2024 Frank Lichtenheld <frank@lichtenheld.com> - 2.7.20240602git82036c1
- Fix false exit status on pre runtime scriptlet (Elkhan Mammadli <elkhan@almalinux.org>, RHBZ#2239722)
- Fix regression of systemctl scriptlet globbing issues (RHBZ#1887984); reintroduced in openvpn-2.6.0-1
- Fix build failure due to "%%patchN is deprecated"
- Fix build failure due to --test-crypto requiring --allow-deprecated-insecure-static-crypto
- Fix build failure due to --enable-systemd breaking UT build
- Bump build dependency to openssl >= 1.1.1

* Sun Jun 02 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20240602git82036c1
- Update to git master 82036c17c45d45c3fe8725f64b33720cb9c94dad

* Sat Jun 01 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20240601gitfbe3b49
- Update to git master fbe3b49b373ea8e81aaa31a383258403a3bfcd07

* Fri May 17 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20240516git55bb326
- Update to git master 55bb3260c12bae33b6a8eac73cbb6972f8517411

* Wed May 15 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20240515git763b35f
- Update to git master 763b35f652b1913ddd01e6c548b3e6a57076ba42

* Tue May 14 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20240514git51f80db
- Update to git master 51f80db910eb48e720ce106b5b9b5ec96d8e0e23

* Mon May 13 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20240513gitb3a271b
- Update to git master b3a271b11723cbe520ad4ce6b4b0459de57ade06

* Thu May 09 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20240509gitd5ba4ac
- Update to git master d5ba4acc297a6041bb45f7aa1c9a99b37b7d5e44

* Mon May 06 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20240506gitbccb22a
- Update to git master bccb22ab44d7e5a60bece286c9daf8b676f2b7c3

* Thu May 02 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20240502git9d92221
- Update to git master 9d92221eb4e773cae913752af6d70082ae305fe8

* Wed May 01 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20240501git066fcdb
- Update to git master 066fcdba9741319fa38cbe40c1761c49727d3f9a

* Wed Apr 03 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20240403git32e6586
- Update to git master 32e6586687a548174b88b64fe54bfae6c74d4c19

* Tue Apr 02 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20240402gite81e3eb
- Update to git master e81e3eb1a4322148b06f353eaa22b0a803fd74f4

* Sun Mar 31 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20240331git4d907bf
- Update to git master 4d907bf46a470ccbd2940b9ecb64d6502d9d86bf

* Fri Mar 29 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20240329git4c71e81
- Update to git master 4c71e816031f564f834df695b3fa717ea22720d2

* Tue Mar 26 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20240326gita94226c
- Update to git master a94226cdc8ed037a6763675aa47e6c821983f174

* Mon Mar 25 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20240325gite8c629f
- Update to git master e8c629fe64c67ea0a8454753be99db44df7ce53e

* Tue Mar 19 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20240319gitfd6b839
- Update to git master fd6b8395f6cee8a61111c28f335ec25ed6db11f7

* Mon Mar 18 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20240318gitb25c6d7
- Update to git master b25c6d7e861d446b7a2e03cbcfb892d554c1ef73

* Fri Mar 15 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20240315git91eb460
- Update to git master 91eb4606a4a3e8e2a4ed2ac4e2257e7ea44ccc44

* Fri Mar 08 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20240308gitf65c656
- Update to git master f65c656ac034a99cca09557eeb9337e7c00a7e73

* Thu Mar 07 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20240307git802fcce
- Update to git master 802fcce5448741bb1e34dd06ac3674b6b6c55a94

* Wed Mar 06 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20240306git15b7403
- Update to git master 15b74036a9b180e862ed4cb23f1e351c08706527

* Tue Mar 05 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20240305gitf8a8c78
- Update to git master f8a8c7879556d2a5d7231eaa911b1a0ad82730a6

* Fri Feb 23 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20240223gitfad2d70
- Update to git master fad2d7017eee366317bb18b34416e7788cbe2372

* Mon Feb 12 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20240212git5447571
- Update to git master 54475711eb119f6fbb263880fca08d4b10df752a

* Sat Feb 10 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20240210git91b057a
- Update to git master 91b057a2b5b4d16b64d9d01824a8ec9327a61da1

* Fri Feb 09 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20240209gitb431721
- Update to git master b431721eb1b676f8e1a1cbcf233507d2dd29f846

* Thu Feb 08 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20240208git70b39f2
- Update to git master 70b39f2bea9fd6e57f31e32b2041246731140cb2

* Wed Feb 07 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20240207git6267693
- Update to git master 62676935d738f74908845ca96819a36a8c0c230e

* Tue Feb 06 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20240206gitc1e1d13
- Update to git master c1e1d132f6368a6f4b77fe956a9329a60331b63e

* Thu Feb 01 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20240201git53b16d0
- Update to git master 53b16d07e889b69128203d3b50ed47ceb77c5771

* Mon Jan 29 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20240129gite1f8c59
- Update to git master e1f8c599aeb840909f5ea8e9ae0bc4dab5bc7deb

* Tue Jan 23 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20240123git7869617
- Update to git master 7869617a0f85089fb5e6fbe2db6f03542a8f33f4

* Mon Jan 22 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20240122gitdc4fde8
- Update to git master dc4fde8052639ffbc29ccc87130a0ce25f6dcd6c

* Fri Jan 19 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20240119gitbc29bd6
- Update to git master bc29bd6a3376158b73d069758122739fbf93c022

* Thu Jan 18 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20240118gitb541a86
- Update to git master b541a86948d7e9866b33e876fcf070fad00b3dce

* Wed Jan 17 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20240117git327355f
- Update to git master 327355f5174772ad2c788aaeb2a7b4db39cff385

* Tue Jan 16 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20240116gitcedbac7
- Update to git master cedbac710c4f6a3c5ecca5487c491f009e2b7775

* Mon Jan 08 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20240108gitf0a17ed
- Update to git master f0a17ed8513405af0efb1df2ab2bda3956e01910

* Sat Jan 06 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20240105git7268e14
- Update to git master 7268e14dba61254b04a0b2db37c12c8ab58423ea

* Fri Jan 05 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20240105gita9fe012
- Update to git master a9fe012ca64d81af37a08666d3e4e74250113db2

* Thu Jan 04 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20240104gitfa79609
- Update to git master fa7960961415fa4f368e9bbb39dc4047680ff30c

* Mon Jan 01 2024 OpenVPN CI <pkg@openvpn.net> - 2.7.20240101git76d1161
- Update to git master 76d11614797617708c31dc3db22e3568fee3de6d

* Sun Dec 31 2023 OpenVPN CI <pkg@openvpn.net> - 2.7.20231231gitf133310
- Update to git master f13331005d5a75f2788685485d46be1fe2f133a1

* Sat Dec 30 2023 OpenVPN CI <pkg@openvpn.net> - 2.7.20231230git21910eb
- Update to git master 21910ebc2ee8a6138eb2af8d38056d2b94e59f9c

* Tue Dec 26 2023 OpenVPN CI <pkg@openvpn.net> - 2.7.20231226git1396072
- Update to git master 139607286ce5d618ece8b17923ce12f418695f4c

* Thu Dec 21 2023 OpenVPN CI <pkg@openvpn.net> - 2.7.20231221gitc590868
- Update to git master c590868a721881dd21bfb77ecf846e6c8720e4ef

* Wed Dec 20 2023 OpenVPN CI <pkg@openvpn.net> - 2.7.20231220git855030f
- Update to git master 855030f3d76324bdb614c05ec0ca5b86d66653bb

* Tue Dec 19 2023 OpenVPN CI <pkg@openvpn.net> - 2.7.20231218git55d7395
- Update to git master 55d73959cc98241d0ef10dbd091ecf0061b175fe

* Tue Dec 12 2023 OpenVPN CI <pkg@openvpn.net> - 2.7.20231212git8656b85
- Update to git master 8656b85c7324fc9ae7f10a9f37227a58766aae33

* Sat Dec 09 2023 OpenVPN CI <pkg@openvpn.net> - 2.7.20231209git3094d8f
- Update to git master 3094d8fb061c56ea73eda00c464fa81cdcd55c94

* Wed Dec 06 2023 OpenVPN CI <pkg@openvpn.net> - 2.7.20231206gitf015643
- Update to git master f015643fe23d7847ad45b7763f31bfc6baed2159

* Tue Dec 05 2023 OpenVPN CI <pkg@openvpn.net> - 2.7.20231205gita78b0e4
- Update to git master a78b0e45dff3a0f0332de47c55aadd76c5919370

* Sat Dec 02 2023 OpenVPN CI <pkg@openvpn.net> - 2.7.20231202git8ba03f9
- Update to git master 8ba03f91388970754f3536866f9687759b07a63c

* Tue Nov 21 2023 OpenVPN CI <pkg@openvpn.net> - 2.7.20231121gita1cb1b4
- Update to git master a1cb1b47b138b9f654cd0bca5de6d08dbca61888

* Mon Nov 20 2023 OpenVPN CI <pkg@openvpn.net> - 2.7.20231120git227799b
- Update to git master 227799b8345128dd3adf2029323457804209fe93

* Fri Nov 17 2023 OpenVPN CI <pkg@openvpn.net> - 2.7.20231117gitb5faf1b
- Update to git master b5faf1b2e90fd44c5137a2b8f3da98c7ae482fc1

* Wed Nov 15 2023 OpenVPN CI <pkg@openvpn.net> - 2.7.20231115gitefad93d
- Update to git master efad93d049c318a3bd9ea5956c6ac8237b8d6d70

* Tue Nov 14 2023 OpenVPN CI <pkg@openvpn.net> - 2.7.20231114git2fcfb77
- Update to git master 2fcfb77a8111cce9308bb893f52ecdb77de91e7c

* Sat Nov 11 2023 OpenVPN CI <pkg@openvpn.net> - 2.7.20231111git20c42b8
- Update to git master 20c42b89f6d38a4426b5fe67f59acaadcb9ac314

* Thu Nov 09 2023 OpenVPN CI <pkg@openvpn.net> - 2.7.20231109git753cba2
- Update to git master 753cba211c4752ef13d810b757f1aa7dee89626b

* Tue Oct 31 2023 OpenVPN CI <pkg@openvpn.net> - 2.7.20231031gitf53f063
- Update to git master f53f06316dbb804128fc5cbee1d8edb274ce81df

* Sun Oct 29 2023 OpenVPN CI <pkg@openvpn.net> - 2.7.20231029gitace7a4f
- Update to git master ace7a4f1c271550bb8ad276663e045ab97a46f16

* Sun Oct 22 2023 OpenVPN CI <pkg@openvpn.net> - 2.7.20231022gitc827f9d
- Update to git master c827f9d83a7246971f435d0053b0252e49770f11

* Thu Oct 19 2023 OpenVPN CI <pkg@openvpn.net> - 2.7.20231019git0068542
- Update to git master 00685421aefcc294581d6e74371e744acdce6bbf

* Wed Oct 18 2023 OpenVPN CI <pkg@openvpn.net> - 2.7.20231018git7c637b3
- Update to git master 7c637b35038e4cc7d2f16e07f413cfa2b0f1f97a

* Sun Oct 15 2023 OpenVPN CI <pkg@openvpn.net> - 2.7.20231015gite8e5f8a
- Update to git master e8e5f8a4c4f8e01dc7317ac87a85d3204882d6bf

* Fri Oct 13 2023 OpenVPN CI <pkg@openvpn.net> - 2.7.20231013gita840d50
- Update to git master a840d5099a7d1a5ceb752c481fc345f6385719df

* Mon Oct 02 2023 OpenVPN CI <pkg@openvpn.net> - 2.7.20231002git2671dcb
- Update to git master 2671dcb69837ae58b3303f11c1b6ba4cee8eea00

* Fri Sep 22 2023 OpenVPN CI <pkg@openvpn.net> - 2.7.20230922git607ae9b
- Update to git master 607ae9b821665dadb6bd0a3ceb6288bda10d5e67

* Thu Sep 21 2023 OpenVPN CI <pkg@openvpn.net> - 2.7.20230921git95cc5fa
- Update to git master 95cc5faa16833acaf12a4d273c5c848984fc73ce

* Wed Sep 20 2023 OpenVPN CI <pkg@openvpn.net> - 2.7.20230920git8ae6c48
- Update to git master 8ae6c48d5d52dec8ec6e47cc1cfe89de9f2ffbcd

* Mon Sep 11 2023 OpenVPN CI <pkg@openvpn.net> - 2.7.20230910git0793eb1
- Update to git master 0793eb105c5720c4eb31af71c9db81459439e510

* Mon Aug 14 2023 Frank Lichtenheld <frank@lichtenheld.com> - 2.7.20230814git5f910a4
- Update to git master 5f910a42b86e90
- Update all packaging from 2.6.x branch
- Bump version to 2.7

* Thu Jan 27 2022 Antonio Quartulli <antonio@openvpn.net> - 2.6.20210127git01ecebf
- Update to git dco 01ecebf1b68cfe

* Thu Aug 13 2020 David Sommerseth <davids@openvpn.net> - 2.5.20200813gitf7432a9
- Update to git master f7432a97fe41ad (essentially v2.5_beta1)

* Sun Jul 19 2020 David Sommerseth <davids@openvpn.net> - 2.5.20200719git4cabd28
- Update to git master 4cabd28a77116a

* Mon Jun 22 2020 David Sommerseth <davids@openvpn.net> - 2.5.20200622git933b4ab
- Update to git master 933b4ab8088918

* Mon May 11 2020 David Sommerseth <davids@openvpn.net> - 2.5.20200511git42fe3e8
- Update to git master 42fe3e8175822a
- Removed the two added quick fix patches, as they are now in the upstream tree
- Added cmocka build dependencies for unit tests

* Fri May 8 2020 David Sommerseth <davids@openvpn.net> - 2.5-20200508git042429d
- Quick fix: options: Fix failing inline tls-auth/crypt with persist-key
- Quick fix: options: Restore --tls-crypt-v2 inline file capability

* Thu May 7 2020 David Sommerseth <davids@openvpn.net> - 2.5-20200507git042429d
- Update to git master 042429d3454ca
- Run unit tests during the check phase

* Fri Apr 24 2020 David Sommerseth <davids@openvpn.net> - 2.5-20200423git75aa88a
- Update to git master 75aa88af774ab

* Sun Mar 22 2020 David Sommerseth <davids@openvpn.net> - 2.5-20200322gite1eb630
- Update to git master e1eb630df1b3e

* Thu Mar  5 2020 David Sommerseth <davids@openvpn.net> - 2.5-20200305git32723d2
- Fix missing packaging of systemd service files on RHEL 7+ and Fedora
- Fix missing packaging of sysv init.d script for RHEL 6

* Wed Mar  4 2020 David Sommerseth <davids@openvpn.net> - 2.5-20200304git32723d2
- Packaging OpenVPN 2.5 git master development snapshot

